﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using KuroLib.Helper;
using PatchOE;

namespace PatchOE_GUI
{
    public class Processor : PatchOE.Processor
    {
        private Form _frm;
        private Label _lb;
        private bool _newLine = false;

        private Action _actRefreshList;

        public Processor(Form frm, Label lb, Action refreshList)
        {
            _frm = frm;
            _lb = lb;
            _actRefreshList = refreshList;
        }

        public override void Write(string str)
        {
            if (_newLine)
            {
                _frm.BeginInvoke(new Action(() => { _lb.Text = null; }));
                _newLine = false;
            }
            if (str.Contains("\n")) _newLine = true;
            if (str.Contains("\b"))
            {
                var count = str.Find("\b");
                _frm.BeginInvoke(new Action(() => { _lb.Text = _lb.Text.Substring(0, _lb.Text.Length - count); }));
                return;
            }
            _frm.BeginInvoke(new Action(() => { _lb.Text += str; }));
        }

        public override void WriteLine(string str)
        {
            _newLine = true;
            _frm.BeginInvoke(new Action(() => { _lb.Text = str; }));
        }

        public override void WriteLine(string[] str)
        {
            foreach (var s in str) WriteLine(s);
        }

        public override void WriteEmpty(string str)
        {
            base.WriteEmpty(str);
        }

        public override bool Confirm(string msg)
        {
            if (MessageBox.Show(_frm, msg, _frm.Text, MessageBoxButtons.YesNo) == DialogResult.Yes) return true;
            return false;
        }

        public override void Abort()
        {
            WriteLine("INITIALIZATION FAILURE, PLEASE RESTART APPLICATION");
        }

        private List<string> _restoreList = new List<string>();
        public object[] RefreshList(bool ignore)
        {
            if (!Directory.Exists(_pathRestore)) return null;
            var zipFiles = Directory.GetFiles(_pathRestore, "zip").ToArray();
            var zips = zipFiles.Select(x => new FileInfo(x)).OrderByDescending(x => x.CreationTime).ToArray();
            var resD = _configSt.RestoreManager.GetByVersion(_verGame);
            if (!ignore) zips = zips.Where(z => resD.FirstOrDefault(r => $"{r.Name}.zip" == z.Name)?.Version == _verGame).ToArray();
            _restoreList.Clear();
            _restoreList.AddRange(zips.Select(z => z.Name.Substring(0, z.Name.Length - 4)).ToArray());
            var result = _restoreList.Select(z => $"({resD.FirstOrDefault(r => r.Name == z)?.Version}){z}").ToArray();
            return result;
        }

        public override void PatchEnd()
        {
            base.PatchEnd();
            _actRefreshList.Invoke();
        }

        public override void RestoreEnd()
        {
            base.RestoreEnd();
            _actRefreshList.Invoke();

        }
    }
}
