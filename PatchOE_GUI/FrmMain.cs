﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using LibGGPK;
using LibGGPK.Records;
using PatchOE;

namespace PatchOE_GUI
{
    public partial class FrmMain : Form
    {
        public Processor Processor;

        public FrmMain()
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;
            Processor = new Processor(this, InfoLabel, RefreshList);

            string workPath = AppDomain.CurrentDomain.BaseDirectory;
#if DEBUG
            workPath = Path.GetFullPath($"{workPath}..\\..\\..\\PatchOE\\bin\\Debug");
#endif
            PatchBtn.MouseClick += (s, e) =>
            {
                new Thread(() =>
                {
                    ControlSwitch(false);
                    Processor.Patch();
                    ControlSwitch(true);
                }).Start();
            };

            RestoreBtn.MouseClick += (s, e) =>
            {
                new Thread(() =>
                {
                    ControlSwitch(false);
                    Processor.Restore(RestoreBox.SelectedIndex == 0 ? null : RestoreBox.Items[RestoreBox.SelectedIndex - 1].ToString(), IgnoreBox.Checked);
                    ControlSwitch(true);
                }).Start();
            };

            IgnoreBox.CheckStateChanged += (s, e) => RefreshList();

            Shown += (s, e) =>
            {
                ControlSwitch(false);
                Processor.Init(workPath);
                Processor.WriteLine($"CREATED BY KUROBEAR  v.{Processor.Version}");
                RefreshList();
                ControlSwitch(true);
            };

            Closing += (s, e) =>
            {
                e.Cancel = false;
                Processor.Exit();
            };
        }

        public void ControlSwitch(bool state)
        {
            PatchBtn.Enabled = state;
            IgnoreBox.Enabled = state;
            RestoreBox.Enabled = state;
            RestoreBtn.Enabled = state;
        }

        public void RefreshList()
        {
            ControlSwitch(false);
            RestoreBox.Items.Clear();
            RestoreBox.Items.Add("All");
            RestoreBox.SelectedIndex = 0;
            var result = Processor.RefreshList(IgnoreBox.Checked);
            if (result != null) RestoreBox.Items.AddRange(result);
            ControlSwitch(true);
        }
    }
}
