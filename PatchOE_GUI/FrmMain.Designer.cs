﻿namespace PatchOE_GUI
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.IgnoreBox = new System.Windows.Forms.CheckBox();
            this.PatchBtn = new System.Windows.Forms.Button();
            this.RestoreBox = new System.Windows.Forms.ComboBox();
            this.RestoreBtn = new System.Windows.Forms.Button();
            this.InfoLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // IgnoreBox
            // 
            this.IgnoreBox.AutoSize = true;
            this.IgnoreBox.Location = new System.Drawing.Point(97, 14);
            this.IgnoreBox.Name = "IgnoreBox";
            this.IgnoreBox.Size = new System.Drawing.Size(75, 21);
            this.IgnoreBox.TabIndex = 0;
            this.IgnoreBox.Text = "忽略版本";
            this.IgnoreBox.UseVisualStyleBackColor = true;
            // 
            // PatchBtn
            // 
            this.PatchBtn.Location = new System.Drawing.Point(11, 10);
            this.PatchBtn.Name = "PatchBtn";
            this.PatchBtn.Size = new System.Drawing.Size(80, 28);
            this.PatchBtn.TabIndex = 1;
            this.PatchBtn.Text = "打补丁";
            this.PatchBtn.UseVisualStyleBackColor = true;
            // 
            // RestoreBox
            // 
            this.RestoreBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.RestoreBox.FormattingEnabled = true;
            this.RestoreBox.Location = new System.Drawing.Point(176, 12);
            this.RestoreBox.Name = "RestoreBox";
            this.RestoreBox.Size = new System.Drawing.Size(154, 25);
            this.RestoreBox.TabIndex = 2;
            // 
            // RestoreBtn
            // 
            this.RestoreBtn.Location = new System.Drawing.Point(330, 11);
            this.RestoreBtn.Name = "RestoreBtn";
            this.RestoreBtn.Size = new System.Drawing.Size(94, 27);
            this.RestoreBtn.TabIndex = 1;
            this.RestoreBtn.Text = "还原";
            this.RestoreBtn.UseVisualStyleBackColor = true;
            // 
            // InfoLabel
            // 
            this.InfoLabel.AutoEllipsis = true;
            this.InfoLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.InfoLabel.Location = new System.Drawing.Point(12, 41);
            this.InfoLabel.Name = "InfoLabel";
            this.InfoLabel.Size = new System.Drawing.Size(411, 22);
            this.InfoLabel.TabIndex = 3;
            this.InfoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FrmMain
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(435, 72);
            this.Controls.Add(this.RestoreBtn);
            this.Controls.Add(this.InfoLabel);
            this.Controls.Add(this.RestoreBox);
            this.Controls.Add(this.PatchBtn);
            this.Controls.Add(this.IgnoreBox);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Patch of Exile GUI";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox IgnoreBox;
        private System.Windows.Forms.Button PatchBtn;
        private System.Windows.Forms.ComboBox RestoreBox;
        private System.Windows.Forms.Button RestoreBtn;
        private System.Windows.Forms.Label InfoLabel;
    }
}

