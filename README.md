## PatchOE (Patch of Exile) 
项目源码托管于码云 [KuroBear/PatchOE](https://gitee.com/KuroBear/PatchOE)  
解包核心功能来源于 [MuxaJIbI4/libggpk](https://github.com/MuxaJIbI4/libggpk)  
此工具仅适用于 [流放之路<font size=1>TENCENT</font>](http://poe.qq.com/) ( *[Path of Exile](https://pathofexile.gamepedia.com/)* )  
下载 [PatchOE](https://gitee.com/KuroBear/PatchOE/attach_files) 位于附件页面内

#### 更新日志
<font size=2>

* *v1.0 <font size=1>(20170915)</font>* 发布
* *v1.2 <font size=1>(20170920)</font>* 修正程序错误
* *v1.3 <font size=1>(20170926)</font>* 修正程序错误
* *v1.4 <font size=1>(20171102)</font>* 新增版本检测与用户界面功能( <font color=red>用新版工具之前最好用旧版回滚补丁再使用新版</font> )
* *v1.5 <font size=1>(20171114)</font>* 支持将文件拖拽至PatchOE.exe直接对游戏打补丁包
* *v1.6 <font size=1>(20171123)</font>* 删除了控制台的`help`指令, 新增了`README.html`文件

</font>

#### 使用说明
**<font color=red>快捷方式</font> <font size=2>直接选定需要打的补丁文件(支持多个文件与文件夹)拖拽到PatchOE.exe上即可直接对游戏进行打补丁操作, 无须额外操作</font>**
* * *
**<font size=2>首先运行`PatchOE.exe`或`PatchOE_GUI.exe`会生成一个Packet文件夹, 将补丁包 **(ZIP)** 放入文件夹内( <font color=red>支持文件夹结构</font> )</font>**  
<font size=2>
PatchOE  
│　config.xml (配置文件)  
│　PatchOE.exe (控制台程序)  
│　PatchOE_GUI.exe (图形界面)  
│  
├─Packet (补丁包目录)  
│ 　│　Packet00.zip  
│ 　│　Packet01.zip  
│ 　│　Packet02.zip  
│ 　│  
│ 　└─Effect (可分文件夹存放)  
│ 　　　　　Packet03.zip  
│ 　　　　　Packet04.zip  
│ 　　　　　Packet05.zip  
│ 　　　　　Packet06.zip  
│  
├─Processed (已打过的补丁包目录)  
└─Restore (备份目录)  
</font>
* * *
**控制台**   
可用 `CTRL+C` 来强制退出控制台

* `patch` (打补丁)  
* `restore [-c|忽略版本]` (还原备份)  
  * `list` (查看备份包信息)  
  例如 : `restore list`
  * `all` (还原所有备份包)  
  例如 : `restore -c all`
  * `-f` (还原单个备份)  
  例如 : `restore -c -f 200000000000`
* `clear`/`cls` (清空控制台)
* `quit`/`exit` (退出程序)

**图形界面**