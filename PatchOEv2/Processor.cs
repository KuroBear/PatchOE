﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using LibGGPK;
using LibGGPK.Records;

namespace PatchOE
{
    public static class Processor
    {
        public static string PathBase;
        public static string PathPced;
        public static string PathBack;
        public static string PathInst;
        public static string PathCent;
        public static string PathTemp;
        public static string PathProc;
        public static GrindingGearsPackageContainer GameContent;
        public static Dictionary<string, FileRecord> GameRecord;

        public static void Restore()
        {

        }

        public static void Start(string gameVersion, string pathBase, string pathTemp, string pathProc,
            string pathInstall, string pathContent, Dictionary<string, string> dictZip,
            KuroLib.Compression.Zip.Compressor compressor, KuroLib.Compression.Zip.Decompressor decompressor, bool restore = false)
        {
            var pwdBackup = "CreatedByBEAR";
            PathBase = pathBase;
            PathPced = Path.Combine(PathBase, "已打");
            PathBack = Path.Combine(PathBase, "备份");
            PathInst = pathInstall;
            PathCent = pathContent;
            PathProc = pathProc;
            PathTemp = Path.Combine(pathTemp, ".back");
            var pathBackInfo = Path.Combine(PathTemp, "info.ppoeinf");
            var pathBackList = Path.Combine(PathTemp, "list.ppoeinf");
            var pathProcInfo = Path.Combine(PathProc, "info.ppoeinf");
            var pathProcList = Path.Combine(PathProc, "list.ppoeinf");
            CreateDirPced();
            CreateDirBack();
            if (!restore)
            {
            }
            else
            {
                Console.WriteLine();
                var resFiles = Directory.GetFiles(PathBack).ToList();
                resFiles.Sort();
                //resFiles = resFiles.Select(x => x.Substring(PathBack.Length + 1)).ToList();
                if (!resFiles.Any())
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(" 目录无备份文件");
                    Console.ForegroundColor = Program.ConsoleDefaultForeColor;
                    return;
                }
                Console.WriteLine(" 请输入下列编号来选择需要还原的备份");
                var dictRes = new Dictionary<string, string>();
                for (int i = 0; i < resFiles.Count; i++)
                {
                    var resFile = resFiles[i];
                    var filename = Path.GetFileName(resFile);
                    var id = i.ToString().PadLeft(3, '0');
                    dictRes.Add(id, resFile);
                    Console.Write(" - ");
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write(id);
                    Console.ForegroundColor = Program.ConsoleDefaultForeColor;
                    Console.Write($" {filename}");
                    Console.Write(Environment.NewLine);
                    //Console.WriteLine($" - {id} {resFile}");
                }
                var idSelected = string.Empty;
                while (true)
                {
                    Console.Write(" 编号:");
                    var stdIn = Console.ReadLine();
                    if (string.IsNullOrEmpty(stdIn)) continue;
                    stdIn = stdIn.Trim();
                    if (dictRes.ContainsKey(stdIn))
                    {
                        idSelected = stdIn;
                        break;
                    }
                }
                if (string.IsNullOrEmpty(idSelected)) return;

                var resDec = decompressor.Decompress(dictRes[idSelected], pathProc, true, pwdBackup);
                if (!resDec)
                {
                    Console.WriteLine();
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(" 备份文件分析失败");
                    Console.ForegroundColor = Program.ConsoleDefaultForeColor;
                    return;
                }
                Console.WriteLine();
                Console.Write(" 备份包信息如下");
                var comment = string.Empty;
                if (File.Exists(pathProcInfo)) comment = File.ReadAllText(pathProcInfo);
                if (string.IsNullOrEmpty(comment)) comment = " - 无";
                else comment = comment.Split('\n').Select(x => x.Trim()).Aggregate("", (curr, c) => curr + $"\n - {c}");
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(comment);
                Console.ForegroundColor = Program.ConsoleDefaultForeColor;
                Console.WriteLine();
                Console.WriteLine(" 输入 y 将开始还原补丁操作, 输入 esc 或 q 停止操作");
                while (true)
                {
                    var key = Console.ReadKey(true);
                    if (key.Key == ConsoleKey.Escape || key.Key == ConsoleKey.Q) return;
                    if (key.Key == ConsoleKey.Y)
                    {
                        File.Delete(pathProcInfo);
                        File.Delete(pathProcList);
                        Console.Clear();
                        Console.WriteLine();
                    }
                }
            }

            Console.WriteLine(" 正在分析游戏文件");
            InitContent();
            var files = KuroLib.StreamIO.File.GetAllFilesByPath(PathProc);
            var bakFiles = files.Select(x => x.Substring(PathProc.Length + 1)).ToArray();
            var repFiles = files.Select(x => x.Substring(PathProc.Length + 1)).ToArray();
            if (!restore)
            {

                Console.WriteLine();
                Console.WriteLine(" 正在将补丁压缩包移动至已打目录");
                foreach (var dictZipKey in dictZip.Keys)
                {
                    Console.Write($" - {dictZipKey} ");
                    try
                    {
                        if (!dictZipKey.StartsWith(PathPced))
                        {
#if !DEBUG
                        var filename = Path.GetFileName(dictZipKey);
                        File.Move(dictZipKey, Path.Combine(PathPced, filename));
#endif
                        }
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write("成功");
                    }
                    catch
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write("失败");
                    }
                    Console.ForegroundColor = Program.ConsoleDefaultForeColor;
                    Console.Write(Environment.NewLine);
                }
                Directory.CreateDirectory(PathTemp);
                Console.WriteLine();
                Console.WriteLine(" 正在备份将要被替换文件");
                foreach (var file in bakFiles)
                {
                    Console.Write($" - {file} ");
                    var path = Path.Combine(PathTemp, file);
                    var dir = Path.GetDirectoryName(path) ?? "";
                    if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);
                    var key = $"ROOT\\{file}";
                    var result = ExtractFile(key, path);
                    if (result)
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write("成功");
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write("失败");
                    }
                    Console.ForegroundColor = Program.ConsoleDefaultForeColor;
                    Console.Write(Environment.NewLine);
                }
                Console.Write(" - 打包备份 ");
                var pathBackZip = $"[{gameVersion}]{DateTime.Now:yyyyMMddHHmmss}.ppoebak";
                pathBackZip = Path.Combine(PathBack, pathBackZip);
                var comment = $"备份时间:{DateTime.Now:yyyy-MM-dd HH:mm:ss}\n" +
                              $"游戏版本:{gameVersion}\n" +
                              $"补丁个数:{dictZip.Keys.Count}\n" +
                              $"补丁列表:{string.Join("|", dictZip.Keys.Select(Path.GetFileName).ToArray())}";
                File.WriteAllText(pathBackInfo, comment);
                File.WriteAllLines(pathBackList, bakFiles);
                compressor.SingleDirectory(PathTemp, PathTemp, $"{pathBackZip}", null, pwdBackup);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write("成功");
                Console.ForegroundColor = Program.ConsoleDefaultForeColor;
                Console.Write(Environment.NewLine);
            }

            Console.WriteLine();
            Console.WriteLine(" 正在替换游戏文件");
            foreach (var file in repFiles)
            {
                Console.Write($" - {file} ");
                var result = true;
#if !DEBUG
                var path = Path.Combine(PathProc, file);
                var key = $"ROOT\\{file}";
                var buffer = File.ReadAllBytes(path);
                result = ReplaceFile(key, buffer);
#endif
                if (result)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write("成功");
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("失败");
                }
                Console.ForegroundColor = Program.ConsoleDefaultForeColor;
                Console.Write(Environment.NewLine);
            }

            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(restore ? " 游戏补丁已还原, 感谢使用本软件" : " 游戏补丁已应用至游戏, 感谢使用本软件");
            Console.ForegroundColor = Program.ConsoleDefaultForeColor;
        }

        public static void InitContent()
        {
            GameContent = new GrindingGearsPackageContainer();
            GameContent.Read(PathCent, WriteEmpty);
            Thread.Sleep(1000);
            GameRecord = new Dictionary<string, FileRecord>(GameContent.RecordOffsets.Count);
            DirectoryTreeNode.TraverseTreePostorder(GameContent.DirectoryRoot, null, n => GameRecord.Add(n.GetDirectoryPath() + n.Name, n as FileRecord));
        }

        public static void WriteEmpty(string str)
        {
            Console.WriteLine(str);
        }

        public static void CreateDirPced()
        {
            if (!Directory.Exists(PathPced)) Directory.CreateDirectory(PathPced);
        }

        public static void CreateDirBack()
        {
            if (!Directory.Exists(PathBack)) Directory.CreateDirectory(PathBack);
        }

        public static bool ExtractFile(string key, string pathSave)
        {
            if (!GameRecord.Keys.Contains(key)) return false;
            GameRecord[key].ExtractFile(PathCent, pathSave);
            return true;
        }

        public static bool ReplaceFile(string key, byte[] buffer)
        {
            if (!GameRecord.Keys.Contains(key)) return false;
            GameRecord[key].ReplaceContents(PathCent, buffer, GameContent.FreeRoot);
            return true;
        }
    }
}
