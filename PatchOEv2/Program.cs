﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Xml;

namespace PatchOE
{
    public static class Program
    {
        public static ConsoleColor ConsoleDefaultForeColor;
        public static ConsoleColor ConsoleDefaultBackColor;
        public static string ContentGgpk = "Content.ggpk";

        private static KuroLib.Compression.Zip.Compressor _compressor;
        private static KuroLib.Compression.Zip.Decompressor _decompressor;
        private static string _gameVersion;

        public static void Main(string[] args)
        {
            // "C:\Code\PatchOE\PatchOEv2\bin\Debug\[3.500.18.1]_功能补丁V7.9.zip"
            var pathBase = AppDomain.CurrentDomain.BaseDirectory;
            Console.CancelKeyPress += (s, e) => { e.Cancel = true; };
            ConsoleDefaultForeColor = Console.ForegroundColor;
            ConsoleDefaultBackColor = Console.BackgroundColor;
            _compressor = new KuroLib.Compression.Zip.Compressor();
            _decompressor = new KuroLib.Compression.Zip.Decompressor();
            var pathTemp = Path.Combine(pathBase, ".temp");
            SpinWait.SpinUntil(() =>
            {
                try { Directory.Delete(pathTemp, true); }
                catch { }
                return !Directory.Exists(pathTemp);
            });
            var pathProc = Path.Combine(pathTemp, ".proc");
            var process = true;

            Console.WriteLine();
            Console.Title = " Patch of Exile";
            Console.WriteLine(" PatchOEv2");
            Console.WriteLine(" 流放之路补丁工具");
            Console.WriteLine(" 此软件用于替换游戏内容(打补丁), 软件本身并不是游戏补丁");
            Console.WriteLine(" 软件由 KuroBear 编写 : https://kuro.vc");
            Console.WriteLine(" 解压类 LibDat 与 LibGPPK 由 MuxaJIbI4 编写 : https://github.com/MuxaJIbI4/libggpk");
            Console.WriteLine(" 软件下载地址 : https://kuro.vc/u/dlpatchoe");
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(" 本软件不承担打完补丁之后出现的任何问题, 若继续使用软件将默认同意此条协议");
            Console.WriteLine(" 下载软件请到上方地址下载, 软件承诺不带任何病毒或恶意程序");
            Console.WriteLine(" 若打完补丁游戏出现游戏内容失常可以随时还原到打补丁之前的状态");
            Console.ForegroundColor = ConsoleDefaultForeColor;
            Console.WriteLine();
            if (SearchGame(pathBase, out var pathInstall))
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine($" 游戏路径 : {pathInstall}");
                if (SearchVersion(pathInstall, out var gameVersion))
                {
                    _gameVersion = gameVersion;
                    Console.WriteLine($" 游戏版本 : {gameVersion}");
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(" 未找到游戏版本");
                    Console.ForegroundColor = ConsoleDefaultForeColor;
                    process = false;
                }
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(" 未找到游戏路径");
                Console.ForegroundColor = ConsoleDefaultForeColor;
                process = false;
            }
            Console.ForegroundColor = ConsoleDefaultForeColor;

            if (process)
            {
                if (args.Length > 0)
                {
                    Console.WriteLine();
                    var zips = args.Where(a => a.EndsWith(".zip")).ToList();
                    var notzips = args.Where(a => !a.EndsWith(".zip")).ToList();
                    if (notzips.Any())
                    {
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine($" 检测到 {notzips.Count} 个文件不是压缩包(.zip)文件");
                        foreach (var notzip in notzips)
                        {
                            var filename = Path.GetFileName(notzip);
                            Console.WriteLine($" - {filename} 被跳过");
                        }
                        Console.ForegroundColor = ConsoleDefaultForeColor;
                    }

                    var dictZip = new Dictionary<string, string>();
                    if (zips.Any())
                    {
                        Directory.CreateDirectory(pathTemp);
                        //if (Directory.Exists(pathZip)) Directory.Delete(pathZip, true);
                        Directory.CreateDirectory(pathProc);
                        zips = zips.OrderBy(z => z).ToList();
                        Console.WriteLine($" 检测到 {zips.Count} 个压缩包");
                        var listWork = new Dictionary<string, string>();
                        foreach (var zip in zips)
                        {
                            //var filename = Path.GetFileName(zip);
                            //var file = filename.Substring(0, filename.LastIndexOf(".", StringComparison.Ordinal));
                            Console.Write($" - {zip} ");
                            var res = false;
                            try
                            {
                                res = _decompressor.Decompress(zip, pathProc, true);
                                if (res) listWork.Add(zip, pathProc);
                            }
                            catch { }
                            if (res)
                            {
                                Console.ForegroundColor = ConsoleColor.Green;
                                Console.Write("解压成功");
                            }
                            else
                            {
                                Directory.Delete(pathProc, true);
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.Write("解压失败");
                            }
                            Console.Write(Environment.NewLine);
                            Console.ForegroundColor = ConsoleDefaultForeColor;
                        }
                        Console.WriteLine();
                        Console.WriteLine(" 正在检测压缩包是否可用(功能未完成)");
                        foreach (var listWorkKey in listWork.Keys)
                        {
                            var checkpass = false;
                            var filename = Path.GetFileName(listWorkKey);
                            if (!string.IsNullOrEmpty(filename))
                            {
                                if (filename.Contains("功能补丁") && filename.Contains("[") && filename.Contains("]"))
                                {
                                    var version = filename.Substring(filename.IndexOf("[") + 1);
                                    version = version.Substring(0, version.IndexOf("]"));
                                    if (version == _gameVersion) checkpass = true;
                                    else
                                    {
                                        Console.Write($" - {listWorkKey} ");
                                        Console.ForegroundColor = ConsoleColor.Yellow;
                                        Console.Write("版本不一致");
                                        Console.Write(Environment.NewLine);
                                        Console.ForegroundColor = ConsoleDefaultForeColor;
                                    }
                                }
                                else
                                {
                                    checkpass = true;
                                }
                            }

                            if (checkpass)
                            {
                                Console.Write($" - {listWorkKey} ");
                                Console.ForegroundColor = ConsoleColor.Green;
                                Console.Write("可用");
                                Console.Write(Environment.NewLine);
                                Console.ForegroundColor = ConsoleDefaultForeColor;
                            }

                            dictZip.Add(listWorkKey, listWork[listWorkKey]);
                        }
                    }

                    if (dictZip.Count != 0)
                    {
                        Console.WriteLine();
                        Console.WriteLine(" 输入 y 将开始打补丁操作, 输入 esc 或 q 停止操作");
                        while (true)
                        {
                            var key = Console.ReadKey(true);
                            if (key.Key == ConsoleKey.Escape || key.Key == ConsoleKey.Q) break;
                            if (key.Key == ConsoleKey.Y)
                            {
                                Console.Clear();
                                Console.WriteLine();
                                Processor.Start(_gameVersion,
                                    pathBase, pathTemp, pathProc,
                                    pathInstall, Path.Combine(pathInstall, ContentGgpk),
                                    dictZip,
                                    _compressor, _decompressor);
                                break;
                            }
                        }

                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine(" 当前无补丁文件可被处理");
                        Console.ForegroundColor = ConsoleDefaultForeColor;
                    }
                }
                else
                {
                    Console.WriteLine();
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(" * 若要使用补丁请勿直接运行软件");
                    Console.WriteLine(" * 将需要打的补丁文件(一般为压缩包)选中拖拽至exe(PatchOE.exe)图标上即可自动处理");
                    Console.ForegroundColor = ConsoleDefaultForeColor;
                    Processor.Start(_gameVersion,
                        pathBase, pathTemp, pathProc,
                        pathInstall, Path.Combine(pathInstall, ContentGgpk),
                        null,
                        _compressor, _decompressor, true);
                }
            }

            try { if (Directory.Exists(pathTemp)) Directory.Delete(pathTemp, true); }
            catch { }
            Console.WriteLine();
            Console.WriteLine(" 回车退出程序");
            Console.ReadLine();
        }

        public static bool SearchGame(string pathBase, out string pathInstall)
        {
            pathInstall = string.Empty;
            var file = Path.Combine(pathBase, ContentGgpk);
            if (File.Exists(file)) pathInstall = pathBase;
            else
            {
                var reg = Microsoft.Win32.Registry.CurrentUser;
                var regObject = reg.OpenSubKey(@"SOFTWARE\Tencent\流放之路");
                if (regObject != null) pathInstall = (string)regObject.GetValue("InstallPath");
            }

            if (!Directory.Exists(pathInstall))
            {
                var installPath = Path.Combine(pathBase, "install.txt");
                pathInstall = File.ReadAllText(installPath, Encoding.Default).Trim();
            }
            return Directory.Exists(pathInstall);
        }

        public static bool SearchVersion(string pathInstall, out string version)
        {
            version = string.Empty;
            var pathConfig = Path.Combine(pathInstall, "TCLS", "mmog_data.xml");
            if (!File.Exists(pathConfig)) return false;
            var pathVersion = pathConfig;
            var xml = new XmlDocument();
            xml.Load(pathVersion);
            var nodeUpd = xml.SelectSingleNode("VersionUpdateData");
            var nodeCliC = nodeUpd.SelectSingleNode("ClientConf");
            var nodeVerD = nodeCliC.SelectSingleNode("VersionData");
            var nodeVers = nodeVerD.SelectSingleNode("Version");
            var strVersion = nodeVers.InnerText;
            version = strVersion;
            return true;
        }
    }
}
