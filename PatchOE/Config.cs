﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace PatchOE
{
    public static class Config
    {
        private static string _pathWork;

        public static CStruct Open(string pathWork = null)
        {
            _pathWork = string.IsNullOrEmpty(pathWork) ? AppDomain.CurrentDomain.BaseDirectory : pathWork;
            var pathFile = $"{_pathWork}\\config.xml";
            var x = Load(pathFile);
            return x ?? Create(pathFile);
        }

        private static CStruct Create(string path, bool overWrite = false)
        {
            if (File.Exists(path) && !overWrite) return null;
            if (File.Exists(path)) File.Delete(path);
            if (File.Exists(path)) return null;
            var doc = new XmlDocument();
            var cs = new CStruct(doc, _pathWork);
            return cs;
        }

        private static CStruct Load(string path)
        {
            if (!File.Exists(path)) return null;
            var doc = new XmlDocument();
            doc.Load(path);
            var cs = new CStruct(doc, _pathWork);
            return cs;
        }
    }
}
