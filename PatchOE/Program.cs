﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Xml;
using KuroLib;
using LibGGPK;
using LibGGPK.Records;

namespace PatchOE
{
    public static class Program
    {
        public static Processor Processor = new Processor();

        public static void Main(string[] args)
        {
            Console.CancelKeyPress += (s, e) => { e.Cancel = true; };
            Console.Title = "Patch of Exile";
            Processor.WriteLine(new[]
            {
                $"*****************************************************************************",
                $"*                            PATCH OF EXILES {Processor.Version}                            *",
                $"**************************** CREATED BY KUROBEAR ****************************",
                $"********************* http://gitee.com/KuroBear/PatchOE *********************",
                $"* LibDat & LibGPPK BY MuxaJIbI4 : https://github.com/MuxaJIbI4/libggpk      *",
                $"*****************************************************************************",
                null,
            });
            Processor.Init(AppDomain.CurrentDomain.BaseDirectory);
            if (args.Length > 0)
            {
                Console.WriteLine();
                var files = KuroLib.StreamIO.File.GetAllFiles(args, "zip");
                Processor.Patch(files, null, true);
                return;
            }
            while (true)
            {
                Processor.Write(">");
                var strLine = Console.ReadLine();
                if (string.IsNullOrEmpty(strLine)) continue;
                Processor.Command(strLine);
            }
        }
    }
}
