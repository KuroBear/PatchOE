﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml;
using LibGGPK;
using LibGGPK.Records;

namespace PatchOE
{
    public enum Command
    {
        Default,
        Patch,
        Restore,
        Help,
        Cls, Clear,
        Quit, Exit,
    }

    public class Processor
    {
        public static string Version => "1.6";

        protected string _pathWork = AppDomain.CurrentDomain.BaseDirectory;
        protected string _pathInstall;
        protected string _pathVersion;
        protected string _pathContent;

        protected string _verGame;

        protected KuroLib.Compression.Zip.Compressor _compressor;
        protected KuroLib.Compression.Zip.Decompressor _decompressor;

        protected bool _initLib = false;
        protected GrindingGearsPackageContainer _gameContent;
        protected Dictionary<string, FileRecord> _gameRecord;
        protected const string ContentFileName = "Content.ggpk";

        protected string _pathPatch;
        protected string _pathPacket;
        protected string _pathRestore;
        protected string _pathProcessed;
        protected CStruct _configSt;

        public Processor() { }

        public void Init(string workPath)
        {
            _pathWork = workPath;

            _configSt = Config.Open(_pathWork);
            _pathPatch = Path.Combine(_pathWork, "Patch");
            _pathPacket = Path.Combine(_pathWork, "Packet");
            _pathRestore = Path.Combine(_pathWork, "Restore");
            _pathProcessed = Path.Combine(_pathWork, "Processed");
            _compressor = new KuroLib.Compression.Zip.Compressor();
            _decompressor = new KuroLib.Compression.Zip.Decompressor();

            if (!Directory.Exists(_pathPacket)) Directory.CreateDirectory(_pathPacket);


            if (!SearchGame()) { Abort(); return; }
            if (!SearchVersion()) { Abort(); return; }
            if (!SearchContent()) { Abort(); return; }


        }

        public void Command(string cmd)
        {
            var command = KuroLib.CommandLine.Parse(cmd);
            var key = command.Params[0].Trim().ToLower();
            var key2 = key.Substring(0, 1).ToUpper() + key.Substring(1);
            Enum.TryParse(key2, out Command comm);
            switch (comm)
            {
                case PatchOE.Command.Patch:
                    Patch();
                    break;
                case PatchOE.Command.Restore:
                    {
                        if (command.Params.Count == 0) break;
                        var ignoreVersion = command.ArgPairs.Keys.Contains("c");
                        if (command.Params.Contains("all")) { Restore(null, ignoreVersion); break; }
                        if (command.Params.Contains("list")) { RestoreList(); break; }
                        if (command.ArgPairs.Keys.Contains("f")) { Restore(command.ArgPairs["f"], ignoreVersion); break; }
                    }
                    break;
                    //case PatchOE.Command.Help:
                    //PrintHelp();
                    break;
                case PatchOE.Command.Clear:
                case PatchOE.Command.Cls:
                    Clear();
                    break;
                case PatchOE.Command.Quit:
                case PatchOE.Command.Exit:
                    Exit();
                    break;
                default:
                    WriteLine("COMMAND NOT FOUND");
                    break;
            }
        }

        public bool SearchGame()
        {
            Write("SEARCHING GAME");
            var reg = Microsoft.Win32.Registry.CurrentUser;
            var regObject = reg.OpenSubKey(@"SOFTWARE\Tencent\流放之路");
            if (regObject != null) _pathInstall = (string)regObject.GetValue("InstallPath");
#if DEBUG
            _pathInstall = Path.Combine(_pathWork);
#endif
            if (string.IsNullOrEmpty(_pathInstall))
            {
                Write(" : NO RESULT\n");
                InputGame();
            }
            else Write(" : FOUND\n");
            return !string.IsNullOrEmpty(_pathInstall);
        }

        public virtual void InputGame()
        {
            for (int i = 0; i < 3; i++)
            {
                WriteLine("PLEASE INPUT INSTALL PATH : ");
                var path = Console.ReadLine();
                if (string.IsNullOrEmpty(path)) continue;
                _pathInstall = path.Trim();
                while (_pathInstall.StartsWith("\"")) _pathInstall = _pathInstall.Substring(1);
                while (_pathInstall.EndsWith("\"")) _pathInstall = _pathInstall.Substring(0, _pathInstall.Length - 1);
                if (!Directory.Exists(_pathInstall))
                {
                    _pathInstall = null;
                    continue;
                }
                Write("\n");
                break;
            }
        }

        public bool SearchVersion()
        {
            Write("SEARCHING VERSION");
            var pathConfig = Path.Combine(_pathInstall, "TCLS", "mmog_data.xml");
            if (!File.Exists(pathConfig))
            {
                Write(" : NO RESULT\n");
                return false;
            }
            _pathVersion = pathConfig;
            var xml = new XmlDocument();
            xml.Load(_pathVersion);
            var nodeUpd = xml.SelectSingleNode("VersionUpdateData");
            var nodeCliC = nodeUpd.SelectSingleNode("ClientConf");
            var nodeVerD = nodeCliC.SelectSingleNode("VersionData");
            var nodeVers = nodeVerD.SelectSingleNode("Version");
            var strVersion = nodeVers.InnerText;
            _verGame = strVersion;
            Write($" : FOUND\n");
            return true;
        }

        public bool SearchContent()
        {
            Write("SEARCHING CONTENT");
            _pathContent = Path.Combine(_pathInstall, ContentFileName);
            if (!File.Exists(_pathContent))
            {
                Write(" : NO RESULT\n");
                return false;
            }
            Write(" : FOUND\n");
            return true;
        }

        public void PrintHelp()
        {
            WriteLine(new[]
            {
                $"********************************** COMMAND **********************************",
                $"patch",
                $"restore",
                $">>>>>>> all [-c|IgnoreVersion]",
                $">>>>>>> -f 20000101000000 [-c|IgnoreVersion]",
                $">>>>>>> list",
                $"cls / clear",
                $"quit / exit",
            });
        }

        public virtual void Write(string str)
        {
            Console.Write(str);
        }

        public virtual void WriteLine(string str)
        {
            Console.WriteLine(str);
        }

        public virtual void WriteLine(string[] str)
        {
            foreach (var s in str) WriteLine(s);
        }

        public virtual void WriteEmpty(string str)
        {

        }

        public bool ExtractFile(string key, string pathSave)
        {
            if (!_gameRecord.Keys.Contains(key)) return false;
            _gameRecord[key].ExtractFile(_pathContent, pathSave);
            return true;
        }

        public bool ReplaceFile(string key, byte[] buffer)
        {
            if (!_gameRecord.Keys.Contains(key)) return false;
            _gameRecord[key].ReplaceContents(_pathContent, buffer, _gameContent.FreeRoot);
            return true;
        }

        public void Clear()
        {
            Console.Clear();
        }

        public void Exit()
        {
            _configSt?.Save();
            Environment.Exit(0);
        }

        public virtual void Abort()
        {
            WriteLine("PRESS ANY KEY TO EXIT");
            Console.ReadKey();
        }

        public virtual bool Confirm(string msg)
        {
            Write(msg);
            var key = ConsoleKey.Process;
            while (key != ConsoleKey.Y && key != ConsoleKey.N)
            {
                var rk = Console.ReadKey(true);
                key = rk.Key;
            }
            Console.Write("\n");
            switch (key)
            {
                case ConsoleKey.Y:
                    return true;
                default:
                case ConsoleKey.N:
                    return false;
            }
        }

        #region Function

        public void InitLib()
        {
            if (_initLib) return;
#if DEBUG
            _pathContent = Path.Combine(_pathWork, ContentFileName);
#endif
            //if (_gameContent != null) return;
            if (!File.Exists(_pathContent))
            {
                WriteLine("CONTENT NOT EXIST");
                return;
            }

            Write("ANALYSING CONTENT");
            _gameContent = new GrindingGearsPackageContainer();
            var keepCount = true;
            new Thread(() =>
            {
                var strDot = "";
                while (keepCount)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        strDot += ".";
                        Write(".");
                        Thread.Sleep(1000);
                    }
                    Write("".PadLeft(strDot.Length, '\b'));
                    Write("".PadLeft(strDot.Length, ' '));
                    Write("".PadLeft(strDot.Length, '\b'));
                    strDot = "";
                }
                Write(" : FINISH\n");
            }).Start();
            _gameContent.Read(_pathContent, WriteEmpty);
            keepCount = false;
            Thread.Sleep(1000);
            _gameRecord = new Dictionary<string, FileRecord>(_gameContent.RecordOffsets.Count);
            DirectoryTreeNode.TraverseTreePostorder(_gameContent.DirectoryRoot, null, n => _gameRecord.Add(n.GetDirectoryPath() + n.Name, n as FileRecord));
            _initLib = true;
        }

        public void Patch()
        {
            var zipFiles = KuroLib.StreamIO.File.GetAllFilesByPath(_pathPacket, "zip").ToArray();
            Patch(zipFiles, _pathPacket);
        }

        public void Patch(string[] zipFiles, string pathPacket, bool allYes = false)
        {
            if (!zipFiles.Any())
            {
                WriteLine("NO PATCH PACKET PACK AVALIABLE");
                return;
            }
            WriteLine("BEGIN PATCH PROGRESS");
            WriteLine(zipFiles.Select(Path.GetFileName).ToArray());
            WriteLine($"TOTAL {zipFiles.Length} FILE(s)");
            if (allYes) PathBegin(zipFiles, pathPacket);
            else
            {
                if (Confirm("DO YOU WANT CONTINUE? [(Y)es|(N)o] : ")) PathBegin(zipFiles, pathPacket);
                else WriteLine("PROCESS ABORT");
            }
        }

        public void PathBegin(string[] zipFiles, string pathPacket, bool allYes = false)
        {
            InitLib();
            WriteLine("BEGIN DECOMPRESS FILE(s)");
            var intFail = 0;
            foreach (var path in zipFiles)
            {
                var fileName = Path.GetFileName(path);
                Write($"{fileName}\t..");
                try
                {
                    _decompressor.Decompress(path, _pathPatch);
                    Write("O");
                }
                catch { Write("X"); intFail++; }
                Write("\n");
            }
            if (intFail > 0)
            {
                if (!allYes)
                {
                    if (!Confirm($"{intFail} FILE(s) DECOMPRESS FAILURE, DO YOU WANT CONTINUE? [(Y)es|(N)o] : "))
                    {
                        WriteLine("PROCESS ABORT");
                        return;
                    }
                }
            }

            if (!Directory.Exists(_pathRestore)) Directory.CreateDirectory(_pathRestore);
            var pathRs = Path.Combine(_pathRestore, $"{DateTime.Now:yyyyMMddHHmmss}");
            var rFiles = KuroLib.StreamIO.File.GetAllFilesByPath(_pathPatch);
            rFiles = rFiles.Select(x => x.Substring(_pathPatch.Length + 1)).ToArray();
            WriteLine("BEGIN BACKUP");
            foreach (var file in rFiles)
            {
                Write($"{file}\t..");
                var pathSave = Path.Combine(pathRs, file);
                var dir = Path.GetDirectoryName(pathSave) ?? "";
                if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);
                var key = $"ROOT\\{file}";
                var result = ExtractFile(key, pathSave);
                Write(result ? "O" : "X");
                Write("\n");
            }
            WriteLine("BEGIN COMPRESS");
            _compressor.SingleDirectory(pathRs, pathRs, $"{pathRs}.zip");
            Directory.Delete(pathRs, true);
            WriteLine("BEGIN REPLACE");
            foreach (var file in rFiles)
            {
                Write($"{file}\t..");
                var pathReplace = Path.Combine(_pathPatch, file);
                var key = $"ROOT\\{file}";
                var buffer = File.ReadAllBytes(pathReplace);
                var result = ReplaceFile(key, buffer);
                Write(result ? "O" : "X");
                Write("\n");
            }
            if (Directory.Exists(_pathPatch)) Directory.Delete(_pathPatch, true);
            if (!Directory.Exists(_pathProcessed)) Directory.CreateDirectory(_pathProcessed);

            var listZip = new List<string>();
            foreach (var zip in zipFiles)
            {
                var z = string.IsNullOrEmpty(pathPacket) ? new FileInfo(zip).Name : zip.Substring(pathPacket.Length + 1);
                var z2 = Path.Combine(_pathProcessed, z);
                var pd = Path.GetDirectoryName(z2);
                if (!Directory.Exists(pd)) Directory.CreateDirectory(pd);
                if (File.Exists(z2)) File.Delete(z2);
                File.Move(zip, z2);
                listZip.Add(z);
            }
            _configSt.RestoreManager.NewRestore(Path.GetFileName(pathRs), _verGame, listZip.ToArray());
            PatchEnd();
        }

        public virtual void PatchEnd()
        {
            WriteLine("PATCH FINISHED");
            _configSt.Save();
        }

        public void Restore(string name = null, bool ignore = false, bool allYes = false)
        {
            if (!Directory.Exists(_pathRestore)) return;
            var zipFiles = string.IsNullOrEmpty(name)
                ? Directory.GetFiles(_pathRestore, "zip").ToArray()
                : new[] { Path.Combine(_pathRestore, $"{name}.zip") };
            if (!zipFiles.Any()) return;
            InitLib();
            WriteLine("BEGIN RESTORE");
            var pathUnzip = Path.Combine(_pathRestore, "Temp");
            var zips = zipFiles.Select(x => new FileInfo(x)).OrderByDescending(x => x.CreationTime).ToArray();
            if (Directory.Exists(pathUnzip)) Directory.Delete(pathUnzip, true);
            var resD = _configSt.RestoreManager.GetByVersion(_verGame);
            if (!ignore) zips = zips.Where(z => resD.FirstOrDefault(r => $"{r.Name}.zip" == z.Name)?.Version == _verGame).ToArray();
            WriteLine("BEGIN DECOMPRESS FILE(s)");
            var intFail = 0;
            foreach (var zip in zips)
            {
                Write($"{zip.Name}\t..");
                try
                {
                    _decompressor.Decompress(zip.FullName, pathUnzip);
                    Write("O");
                }
                catch { Write("X"); intFail++; }
                Write("\n");
            }
            if (intFail > 0)
            {
                if (!allYes)
                {
                    if (!Confirm($"{intFail} FILE(s) DECOMPRESS FAILURE, DO YOU WANT CONTINUE? [(Y)es|(N)o] : "))
                    {
                        WriteLine("PROCESS ABORT");
                        return;
                    }
                }
            }
            var rFiles = KuroLib.StreamIO.File.GetAllFilesByPath(pathUnzip);
            rFiles = rFiles.Select(x => x.Substring(pathUnzip.Length + 1)).ToArray();
            foreach (var file in rFiles)
            {
                Write($"{file}\t..");
                var pathReplace = Path.Combine(pathUnzip, file);
                var key = $"ROOT\\{file}";
                var buffer = File.ReadAllBytes(pathReplace);
                var result = ReplaceFile(key, buffer);
                Write(result ? "O" : "X");
                Write("\n");
            }
            if (Directory.Exists(pathUnzip)) Directory.Delete(pathUnzip, true);
            RestoreEnd();
        }

        public virtual void RestoreEnd()
        {
            WriteLine("RESTORE FINISHED");
            _configSt.Save();
        }

        public void RestoreList()
        {
            if (!Directory.Exists(_pathRestore))
            {
                WriteLine("NO RESTORE PACKET AVALIABLE");
                return;
            }
            var zipFiles = Directory.GetFiles(_pathRestore, "zip").ToArray();
            var zips = zipFiles.Select(x => new FileInfo(x)).OrderByDescending(x => x.CreationTime).ToArray();
            var resD = _configSt.RestoreManager.GetByVersion(_verGame);
            foreach (var zip in zips)
            {
                var v = resD.FirstOrDefault(r => $"{r.Name}.zip" == zip.Name);
                WriteLine($"NAME:{v?.Name ?? zip.Name.Substring(0, zip.Name.Length - 4)}\t VERSION:{v?.Version ?? "_UNKNOW_"}\t FILE(s):{v?.Files?.Count ?? 0}");
                var fss = v?.Files;
                if (fss?.Count > 0)
                {
                    foreach (var fs in fss) WriteLine($"\t{fs.PathOrigin}");
                }
            }
        }

        #endregion
    }
}
