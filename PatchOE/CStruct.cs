﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace PatchOE
{
    public class CStruct
    {
        private string _pathWork;
        private XmlDocument _doc;
        public CRestoreManager RestoreManager;

        private XmlNode _nodeRestore;

        public CStruct(XmlDocument doc, string pathWork)
        {
            pathWork = string.IsNullOrEmpty(pathWork) ? AppDomain.CurrentDomain.BaseDirectory : pathWork;
            _pathWork = pathWork;
            var nodeConfig = doc.SelectSingleNode("config");
            if (nodeConfig == null)
            {
                nodeConfig = doc.CreateElement("config");
                doc.AppendChild(nodeConfig);
            }

            var nodeRestore = nodeConfig.SelectSingleNode("restore");
            if (nodeRestore == null)
            {
                nodeRestore = doc.CreateElement("restore");
                nodeConfig.AppendChild(nodeRestore);
            }
            _nodeRestore = nodeRestore;
            RestoreManager = new CRestoreManager(nodeRestore.ChildNodes);

            _doc = doc;
        }

        public bool RemoveRestoreSection(string name)
        {
            var rm = _nodeRestore.ChildNodes.Cast<XmlElement>().FirstOrDefault(n => n.GetAttribute("name") == name);
            var result = rm?.ParentNode?.RemoveChild(rm);
            return result != null;
        }

        public bool Save()
        {
            var nonSaved = RestoreManager.NonSaved();
            foreach (var res in nonSaved)
            {
                var sec = _doc.CreateElement("section");
                sec.SetAttribute("name", res.Name);
                sec.SetAttribute("version", res.Version);
                foreach (var f in res.Files)
                {
                    var nodeFile = _doc.CreateElement("file");
                    nodeFile.SetAttribute("pathOrigin", f.PathOrigin);
                    nodeFile.SetAttribute("pathProcessed", f.PathProcessed);
                    sec.AppendChild(nodeFile);
                }
                _nodeRestore.AppendChild(sec);
            }
            _doc.Save($"{_pathWork}\\config.xml");
            return true;
        }
    }

    public class CRestoreManager
    {
        private List<CRestore> _rs;

        public CRestoreManager(XmlNodeList nodes)
        {
            if (nodes?.Count > 0) _rs = nodes.Cast<XmlNode>().Select(n => new CRestore(n)).ToList();
            else _rs = new List<CRestore>();
        }

        public List<CRestore> GetByVersion(string version)
        {
            return _rs.Where(r => r.Version == version).ToList();
        }

        public bool NewRestore(string name, string version, string[] files)
        {
            if (files.Length == 0) return false;
            var res = new CRestore(name, version, files);
            _rs.Add(res);
            return false;
        }

        public List<CRestore> NonSaved()
        {
            return _rs.Where(r => r.Log == false).ToList();
        }

        public List<CRestore> All => _rs;
    }

    public class CRestore
    {
        public readonly List<CFile> Files;
        public readonly string Name;
        public readonly string Version;
        public bool Log = true;

        public CRestore(string name, string version, string[] files)
        {
            Name = name;
            Version = version;
            Files = files.Select(f => new CFile(f, f)).ToList();
            Log = false;
        }

        public CRestore(XmlNode node)
        {
            Name = node.Attributes?["name"].Value;
            Version = node.Attributes?["version"].Value;
            Files = node.ChildNodes.Cast<XmlNode>().Select(n => new CFile(n)).ToList();
        }
    }

    public class CFile
    {
        public readonly string PathOrigin;
        public readonly string PathProcessed;

        public CFile(string pathOrigin, string pathProcessed)
        {
            PathOrigin = pathOrigin;
            PathProcessed = pathProcessed;
        }

        public CFile(XmlNode node)
        {
            PathOrigin = node.Attributes?["pathOrigin"].Value;
            PathProcessed = node.Attributes?["pathProcessed"].Value;
        }
    }
}
